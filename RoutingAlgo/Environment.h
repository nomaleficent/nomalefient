//
//  Environment.h
//  RoutingAlgo
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_Environment_h
#define RoutingAlgo_Environment_h

// Here you can find system environment variables and parameters

#ifndef INT_MAX
    #define INT_MIN     (-2147483647 - 1) /* minimum (signed) int value */
    #define INT_MAX       2147483647    /* maximum (signed) int value */
#endif

#ifndef MAXFLOAT
	#define MAXFLOAT	0x1.fffffep+127f
#endif

#endif
