//
//  ServiceFunctions.h
//  RoutingAlgo
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_ServiceFunctions_h
#define RoutingAlgo_ServiceFunctions_h

#include "DataModel.h"
#include <math.h>
//#include "NetworkParameters.h"

// Clear math
double normalizedRand() {
    return (double)(rand() % 100) / (double)100;
}

// Real-timing functions
double energyConsumptionOnSending(double distance, int dataLength, int nodeIndex) {
    return dataLength * (electronicConsumption(nodeIndex) + amplifierConsumption(nodeIndex) * distance * distance);
}

double energyConsumptionOnReceiving(int dataLength, int nodeIndex) {
    return dataLength * electronicConsumption(nodeIndex);
}

double durationOfOperation(int dataLength) {
    return dataLength / TRANSFER_SPEED;
}

// Routing functions
// distance from node "from" to node "to"
double distance(int fromIndex, int toIndex) {
    return (sqrt(pow(node[fromIndex].x - node[toIndex].x, 2) + pow(node[fromIndex].y - node[toIndex].y, 2)));
}

// Buffer size
int getRemainBuffer(int nodeIndex) {
    return (node[nodeIndex].bufferSize - node[nodeIndex].cqsize);
}

int amountOfNodesInSensitiveArea(int nodeIndex) {
    int amountOfNodes = 0;
    for (int i = 0; i < currentNodesAmount; i++) {
        if (i != nodeIndex) {
            if (node[nodeIndex].isLive) {
                if (distance(nodeIndex, i) < MAX_TRANSFER_DIS) {
                    amountOfNodes++;
                }
            }
        }
    }
    return amountOfNodes;
}

double density(int nodeIndex) {
    int amountOfNeighboringNodes = amountOfNodesInSensitiveArea(nodeIndex);
    double areaOfNetwork = WIDTH * HEIGHT;
    double returnValue = (amountOfNeighboringNodes * areaOfNetwork) / (currentNodesAmount * M_PI * pow(MAX_TRANSFER_DIS, 2));
    return returnValue;
}

// Hop count calculation
void updateHopCount() {
    for (int i = 0; i < SINK_N; i++) {
        node[i].hopCount = 0;
    }
    for (int i = SINK_N; i < currentNodesAmount; i++) {
        node[i].hopCount = INT_MAX;
    }
    bool nextRoundRequired = false;
    do {
        nextRoundRequired = false;
        bool shouldCheckHopCounts = false;
        for (int i = 0; i < currentNodesAmount; i++) {
            if (node[i].hopCount == INT_MAX) {
                for (int j = 0; j < currentNodesAmount; j++) {
                    if (i != j) {
                        if (distance(i, j) < MAX_TRANSFER_DIS) {
                            if (node[j].isLive) {
                                if (node[j].hopCount != INT_MAX) {
                                    if (node[i].hopCount > (node[j].hopCount + 1)) {
                                        node[i].hopCount = (node[j].hopCount + 1);
                                        shouldCheckHopCounts = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (shouldCheckHopCounts) {
            for (int i = 0; i < currentNodesAmount; i++) {
                if (node[i].hopCount == INT_MAX) {
                    nextRoundRequired = true;
                }
            }
        }
    } while (nextRoundRequired);
}

// Hop count calculation
void updateEnergyCostValue() {
    for (int i = 0; i < SINK_N; i++) {
        node[i].energyCost = 0;
    }
    for (int i = SINK_N; i < currentNodesAmount; i++) {
        node[i].energyCost = MAXFLOAT;
    }
    bool nextRoundRequired = false;
    do {
        nextRoundRequired = false;
        for (int i = 0; i < currentNodesAmount; i++) {
            double minimalEnergyCost = node[i].energyCost;
            for (int j = 0; j < currentNodesAmount; j++) {
                if (i != j) {
                    if (distance(i, j) < MAX_TRANSFER_DIS) {
                        if (node[j].isLive) {
                            if (node[j].energyCost < node[i].energyCost) {
                                double transitionDistance = distance(i, j);
                                double countedEnergyCost = node[j].energyCost + energyConsumptionOnSending(transitionDistance, 1, i);
                                if (countedEnergyCost < minimalEnergyCost) {
                                    minimalEnergyCost = countedEnergyCost;
                                }
                            }
                        }
                    }
                }
            }
            if (minimalEnergyCost < node[i].energyCost) {
                node[i].energyCost = minimalEnergyCost;
                nextRoundRequired = true;
            }
        }
    } while (nextRoundRequired);
}

#endif
