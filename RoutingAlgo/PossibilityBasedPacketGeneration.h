//
//  PossibilityBasedPacketGeneration.h
//  RoutingAlgo
//
//  Created by admin on 8/6/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_PossibilityBasedPacketGeneration_h
#define RoutingAlgo_PossibilityBasedPacketGeneration_h

#include "NetworkParameters.h"
#include "DataModel.h"
#include "ServiceFunctions.h"

#define INPUT_PACKETS_PERIOD 0.2

void initialInputSettings() {
    for (int i = 0; i < currentNodesAmount; i++) {
        node[i].timeOfLastPacketArrival = - (normalizedRand() * INPUT_PACKETS_PERIOD);
    }
}

void inputPacketsIfNeededForCurrentTime(double currentTime) {
    for (int i = SINK_N; i < currentNodesAmount; i++) { // We don't generate packets on sink nodes which are first SINK_N nodes in array
        // As described in method 8.3
        // possibility_of_packet = (T_current - T_lastAddingPacket) * random(0, 1) / T_period.
        // if (possibility_of_packet > common_possibility) -> add packet to this node.
        if (node[i].isLive) {
            double packetPossibility = normalizedRand() * (currentTime - node[i].timeOfLastPacketArrival) / INPUT_PACKETS_PERIOD;
            if (packetPossibility > PACK_INIT_RATIO) {
                int inputPacketSize = rand() % MAX_PACKET_SIZE;
                if ((node[i].bufferSize > (node[i].cqsize + inputPacketSize)) && node[i].isLive) {
                    // Node can accept input packet
                    node[i].cqsize += inputPacketSize;
                    node[i].timeOfLastPacketArrival = currentTime;
                    totalArrivalDataSize += inputPacketSize;
                } else {
                    totalLostPacketInArrive += inputPacketSize;
                }
            }
        }
    }
}


#endif
