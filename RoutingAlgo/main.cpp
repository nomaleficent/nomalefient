//
//  main.cpp
//  RoutingAlgo
//
//  Created by admin on 7/13/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//


#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>

#include "Environment.h"
#include "FileOperations.h"
#include "NetworkParameters.h"
#include "DataModel.h"
#include "ServiceFunctions.h"

#include "Timer.h"
#include "DataModelExecution.h"
#include "ResultGathering.h"
#include "Router.h"

// This should be moved to algorithm implementation
void ProceedRound(double currentTime) {
    // Here we should do all we need on currentTime change
    
    // Discharge waiting nodes
    dischargeWaitingNodes();
    
    // First of all - flush data transitions
    flushCompletedTransitions(currentTime);
    
    // handle nodes amount changes - crashes and deploys
    for (int i = 0; i < NODE_AMOUNT_CHANGES_IN_EXPERIMENT; i++) {
        if (fabs(changeTimes[i] - currentTime) < TIME_QUANT/2) { // Specific evaluation
            int nodeDelta = nodesDeltas[i];
            if (nodeDelta < 0) {
                crashNodes(-nodeDelta);
            } else {
                deployNodes(nodeDelta);
            }
        }
    }
    
    // Validate if we have some dead nodes
    validateDeadNodes();
    
    // Check for first node death
    if (firstNodeDeathOccured < 0) { // Initial value is -1, as it had been changed, we should stop this check
        checkForFirstDeathOccured(currentTime);
    }
    
    // Update Hop count and energy cost to react on changes in network topology in real time
    updateHopCount();
    updateEnergyCostValue();
    
    // Generate input packets
    inputPacketsIfNeededForCurrentTime(currentTime);
    
    // Actually execute routing algorithm
    initiateNegotiations(currentTime);
    
    // Gather statistics
    PrintStatisticsAtTimeQuant(currentTime);
}

int main() {
    // Reset for srand
    srand((unsigned int)time(NULL));
    
    // Open needed files for statistics gathering
    Init_OutFile();
    
    // Real-timing execution
    double currentTime = 0;
    nodesInitialSetup();
    initialInputSettings();
    do {
        ProceedRound(currentTime);
        currentTime = Tick();
    } while (currentTime != 0);
    
    // Close files for statistics gathering
    Close_OutFile();
    return 0;
}

