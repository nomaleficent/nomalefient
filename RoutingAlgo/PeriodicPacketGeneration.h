//
//  PeriodicPacketGeneration.h
//  RoutingAlgo
//
//  Created by admin on 8/6/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_PeriodicPacketGeneration_h
#define RoutingAlgo_PeriodicPacketGeneration_h

#include "NetworkParameters.h"
#include "DataModel.h"
#include "ServiceFunctions.h"

#define INPUT_PACKETS_PERIOD 0.2

void initialInputSettings() {
    // Do nothing for this method
}

void inputPacketsIfNeededForCurrentTime(double currentTime) {
    static double lastInputTime = 0;
    if ((lastInputTime != 0) && ((lastInputTime + INPUT_PACKETS_PERIOD) > currentTime)) {
        // We don't need to generate packets yet
        return;
    }
    
    lastInputTime = currentTime;
    for (int i = SINK_N; i <currentNodesAmount; i++) { // We don't generate packets on sink nodes which are first SINK_N nodes in array
        if (node[i].isLive) {
            double packetPossibility = normalizedRand();
            if (packetPossibility <= PACK_INIT_RATIO) {
                int inputPacketSize = rand() % MAX_PACKET_SIZE;
                if ((node[i].bufferSize > (node[i].cqsize + inputPacketSize)) && node[i].isLive) {
                    // Node can accept input packet
                    node[i].cqsize += inputPacketSize;
                    totalArrivalDataSize += inputPacketSize;
                } else {
                    totalLostPacketInArrive += inputPacketSize;
                }
            }
        }
    }
}


#endif
