//
//  FileOperations.h
//  RoutingAlgo
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_FileOperations_h
#define RoutingAlgo_FileOperations_h

// Here is the unit for file operations, used in project

FILE *fresult;
FILE *fanalysis;
FILE *fnegotiations;

void Init_OutFile() {
    fresult = fopen("Result_ResidualEnergy400.dat", "w");
    fanalysis = fopen("Analysis_ResidualEnergy400.dat", "w");
    fnegotiations = fopen("Negotiations_ResidualEnergy400.dat", "w");
    
    fprintf(fresult, "current time\t throughput\t energyEfficiency\n");
}

void Close_OutFile() {
    fclose(fresult);
    fclose(fanalysis);
    fclose(fnegotiations);
}

#endif
