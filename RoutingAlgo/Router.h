//
//  Router.h
//  RoutingAlgo
//
//  Created by admin on 8/13/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_Router_h
#define RoutingAlgo_Router_h

#include "FileOperations.h"

// Here you'll find routing implementation and gathering information on decision making
bool nodeIsRequestor(int nodeIndex) {
    if (nodeIndex < SINK_N) { // First SINK_N nodes can't be requestors as they are sink nodes
        // Sink node can't be requestor
    } else {
        if ((node[nodeIndex].nodeState == WAITING) && node[nodeIndex].isLive) { // Node has correct state and live
            if (node[nodeIndex].cqsize > 0) {
                // Node has data to send
                return true;
            }
        }
    }
    return false;
}

bool nodeCanBeProvider(int requestorNodeIndex, int providerNodeIndex) {
    if (requestorNodeIndex != providerNodeIndex) { // Not the same node
        if ((node[providerNodeIndex].nodeState == WAITING) && node[providerNodeIndex].isLive) {
            if (getRemainBuffer(providerNodeIndex) > 0) { // Has some free space
                if (distance(requestorNodeIndex, providerNodeIndex) < MAX_TRANSFER_DIS) { // Distance smaller then coverage range
                    return true;
                }
            }
        }
    }
    return false;
}

double optimalityForTransition(int requestorNodeIndex, int providerNodeIndex) {
//    Optimality = ((weightOfFreeBuffer * freeBytesInProviderBuffer) +
//                  (weightOfHopCount / hopCountOfProvider) +
//                  (weightOfEnergy * amountOfAvailableEnergyOnProvider) +
//                  (weightOfDensity * densityOfProvider))
    int freeBytesInProviderBuffer = getRemainBuffer(providerNodeIndex);
    int hopCountOfProvider = node[providerNodeIndex].hopCount;
    double hopCountCoefficient = 0;
    if (providerNodeIndex < SINK_N) {
        hopCountCoefficient = MAXFLOAT;
    } else {
        hopCountCoefficient = 1.0f / hopCountOfProvider;
    }
    double amountOfAvailableEnergyOnProvider = node[providerNodeIndex].energy;
    double densityOfProvider = density(providerNodeIndex);
    double energyCostDifference = node[requestorNodeIndex].energyCost - node[providerNodeIndex].energyCost;
    fprintf(fnegotiations, "Optimality from %d to %d : free memory : %d hopCount : %d energy on provider : %03.4f density : %03.4f", requestorNodeIndex, providerNodeIndex, freeBytesInProviderBuffer, hopCountOfProvider, amountOfAvailableEnergyOnProvider, densityOfProvider);
    double optimality = ((weightOfFreeBuffer * freeBytesInProviderBuffer) +
                         (weightOfHopCount * hopCountCoefficient) +
                         (weightOfEnergy * amountOfAvailableEnergyOnProvider) +
                         (weightOfDensity * densityOfProvider) +
                         (weightOfEnergyCost * energyCostDifference));
    return optimality;
}


void initiateNegotiations(double currentTime) {
    for (int requestorIndex = 0; requestorIndex < currentNodesAmount; requestorIndex++) {
        // Validate if node is requestor
        bool nodeShouldBeRequestor = nodeIsRequestor(requestorIndex);
        if (nodeShouldBeRequestor) {
            
            // As this node should be requestor - select optimal provider for it
            int selectedProvider = INVALID_V;
            double selectedProviderOptimality = INVALID_V;
            fprintf(fnegotiations, "initiate provider selection for requestor : %d", requestorIndex);
            for (int providerIndex = 0; providerIndex < currentNodesAmount; providerIndex++) {
                // Validate if other node can be provider
                bool nodeShouldBeProvider = nodeCanBeProvider(requestorIndex, providerIndex);
                if (nodeShouldBeProvider) {
                    double optimality = optimalityForTransition(requestorIndex, providerIndex);
                    if (optimality > selectedProviderOptimality) {
                        selectedProviderOptimality = optimality;
                        selectedProvider = providerIndex;
                    }
                }
            }
            
            // Check if any provider found
            if (selectedProvider != INVALID_V) {
                // We have found some provider
                fprintf(fnegotiations, "selected provider : %d with optimality value : %03.4f", selectedProvider, selectedProviderOptimality);
                // Calculate amount of data, which we can send
                int dataLengthForTransition = amountOfDataCanBeSent(requestorIndex, selectedProvider);
                
                // Setup data transition
                setupDataTransition(requestorIndex, selectedProvider, dataLengthForTransition, currentTime);
            } else {
                fprintf(fnegotiations, "provider was not found");
            }
        }
    }
}

#endif
