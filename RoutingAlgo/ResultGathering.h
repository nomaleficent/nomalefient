//
//  ResultGathering.h
//  RoutingAlgo
//
//  Created by admin on 7/20/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_ResultGathering_h
#define RoutingAlgo_ResultGathering_h

#include "FileOperations.h"
#include "DataModel.h"

/*
 * @brief Print statistics number to file for future analysis.
 * @note  write throughput & energyEfficient to Result.dat. write CH node, liveNode, deadNode, energy cost & packet Lost to Analysis.dat
 */
void PrintStatisticsAtTimeQuant(double currentTime)
{
    double energyEfficiency, throughput, lostRatio;
    int sss;
    int liveCnt = 0;
    
    /**** print to analysis file ****/
    fprintf(fanalysis, "%03.4f] \n", currentTime);
    fprintf(fanalysis, "remain energy     : ");
    printf("remain energy: \n");
    for(sss = 0; sss < currentNodesAmount; sss++) {
        fprintf(fanalysis, "%0.2f ", node[sss].energy);
        printf(" %0.2f", node[sss].energy);
        if (node[sss].isLive) {
            liveCnt++;
        }
    }
    fprintf(fanalysis, "\n\n");
    fprintf(fanalysis, "live node : %d", liveCnt);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "dead node : %d\n", (currentNodesAmount - liveCnt));
    fprintf(fanalysis, "\n");
    if (firstNodeDeathOccured > 0) {
        fprintf(fanalysis, "First node death occured at : %f from modelling started\n", firstNodeDeathOccured);
    }
    fprintf(fanalysis, "total energy used in Transfer : %f J ", totalTransferEnergy);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total energy used in Receive : %f J",  totalReceiveEnergy);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total energy wasted during Wait: %f J\n", totalWaitingEnergyWaste);
    fprintf(fanalysis, "Sum : " "%f J\n", totalTransferEnergy + totalReceiveEnergy + totalWaitingEnergyWaste);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total Bytes lost in transfer  : %ld B ",  totalLostPacketInTransfer);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total Bytes lost in arrive : %ld B\n", totalLostPacketInArrive);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total Bytes arrived in sink   : %ld B", totalArriveToSinkSize);
    fprintf(fanalysis, "\n");
    fprintf(fanalysis, "total Bytes arrived : %ld B\n\n", totalArrivalDataSize);
    
    /**** print to result file ****/
    if (totalArriveToSinkSize == 0) {
        lostRatio = 0;
        energyEfficiency = 0;
    } else {
        lostRatio = (double)(totalLostPacketInTransfer + totalLostPacketInArrive) / (double)totalArriveToSinkSize;
        energyEfficiency = (double)(totalReceiveEnergy + totalTransferEnergy) / (double)totalArriveToSinkSize;
    }
    throughput = (double)1 - lostRatio;
    
    fprintf(fresult, "%.4f\t   %0.5f\t   %0.5f\n ", currentTime, throughput, energyEfficiency);
    printf("\nResult output: %.4f\t   %0.5f\t   %0.5f\n ", currentTime, throughput, energyEfficiency);
    printf("%03.4f) dead Cnt : %02d\n", currentTime, (currentNodesAmount - liveCnt));
}

#endif
