//
//  AsyncPeriodicPacketGeneration.h
//  RoutingAlgo
//
//  Created by admin on 8/6/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_AsyncPeriodicPacketGeneration_h
#define RoutingAlgo_AsyncPeriodicPacketGeneration_h

#include "NetworkParameters.h"
#include "DataModel.h"
#include "ServiceFunctions.h"

#define INPUT_PACKETS_PERIOD 0.2

void initialInputSettings() {
    for (int i = 0; i < currentNodesAmount; i++) {
        node[i].timeOfLastPacketArrival = - (normalizedRand() * INPUT_PACKETS_PERIOD);
    }
}

void inputPacketsIfNeededForCurrentTime(double currentTime) {
    for (int i = SINK_N; i < currentNodesAmount; i++) { // First SINK_N nodes are sink nodes, where we don't generate packets
        if (node[i].isLive) {
            if (node[i].timeOfLastPacketArrival + INPUT_PACKETS_PERIOD <= currentTime) {
                int inputPacketSize = rand() % MAX_PACKET_SIZE;
                if ((node[i].bufferSize > (node[i].cqsize + inputPacketSize)) && node[i].isLive) {
                    // Node can accept input packet
                    node[i].cqsize += inputPacketSize;
                    node[i].timeOfLastPacketArrival = currentTime;
                    totalArrivalDataSize += inputPacketSize;
                } else {
                    totalLostPacketInArrive += inputPacketSize;
                }
            }
        }
    }
}


#endif
