//
//  DataModelExecution.h
//  RoutingAlgo
//
//  Created by admin on 7/20/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_DataModelExecution_h
#define RoutingAlgo_DataModelExecution_h

#include "DataModel.h"
#include "ServiceFunctions.h"

// Packet generation selection
#include "PeriodicPacketGeneration.h" // Packet generation described as 8.1
//#include "AsyncPeriodicPacketGeneration.h" // Packet generation described as 8.2
//#include "PossibilityBasedPacketGeneration.h" // Packet generation described as 8.3

// Here you'll find functions which are executing data model, like updating energy, etc.

/* 
 * @brief Here each node characteristic is setting up before running routing algo
 */

void singleNodeSetup(int nodeIndex) {
    node[nodeIndex].x = rand() % WIDTH;
    node[nodeIndex].y = rand() % HEIGHT;
    node[nodeIndex].energy = initialEnergy(nodeIndex);
    node[nodeIndex].bufferSize = initialBufferSize(nodeIndex);
    node[nodeIndex].cqsize = 0;
    node[nodeIndex].isLive = true;
    node[nodeIndex].nodeState = WAITING;
    node[nodeIndex].interactingNodeIndex = INVALID_V;
    node[nodeIndex].transactionDataLength = 0;
    transfer_energy[nodeIndex] = 0;
    receive_energy[nodeIndex] = 0;
    bits_send[nodeIndex] = 0;
}

void nodesInitialSetup() {
    for(int i = 0; i < currentNodesAmount; i++) {
        singleNodeSetup(i);
    }
    
    // Let first sink node be in center
    node[0].x = WIDTH / 2;
    node[0].y = HEIGHT / 2;
    totalArriveToSinkSize = 0;
}

void deployNode() {
    int newNodeIndex = currentNodesAmount;
    currentNodesAmount++;
    singleNodeSetup(newNodeIndex);
}

bool nodeCanBeCrahed(int nodeIndex) {
    if (nodeIndex < SINK_N) {
        return false;
    }
    if (!node[nodeIndex].isLive) {
        return false;
    }
    return true;
}

void crashNode() {
    int nodeIndexToCrash = 0;
    while (!nodeCanBeCrahed(nodeIndexToCrash)) {
        nodeIndexToCrash = rand() % currentNodesAmount;
    }
    node[nodeIndexToCrash].energy = 0;
    node[nodeIndexToCrash].isLive = false;
}

void deployNodes(int nodesDelta) {
    for (int i = 0; i < nodesDelta; i++) {
        deployNode();
    }
}

void crashNodes(int nodesAmount) {
    for (int i = 0; i < nodesAmount; i++) {
        crashNode();
    }
}

void dischargeWaitingNodes() {
    for (int i = SINK_N; i < currentNodesAmount; i++) {
        // We don't discharge sink nodes
        if (node[i].isLive) {
            // We don't discharge dead nodes
            if (node[i].nodeState == WAITING) {
                double waitingEnergyWaste = waitingConsumption(i) * TIME_QUANT;
                node[i].energy -= waitingEnergyWaste;
                totalWaitingEnergyWaste +=waitingEnergyWaste;
            }
        }
    }
}

void setupDataTransition(int fromNodeIndex, int toNodeIndex, int dataLength, double currentTime) {
    double transitionDuration = durationOfOperation(dataLength);
    double transitionDistance = distance(fromNodeIndex, toNodeIndex);
    
    // Setting up state on sending node
    node[fromNodeIndex].nodeState = SENDING;
    node[fromNodeIndex].interactingNodeIndex = toNodeIndex;
    node[fromNodeIndex].endOfInteraction = currentTime + transitionDuration;
    node[fromNodeIndex].transactionDataLength = dataLength;
    
    // Setting up state on receiving node
    node[toNodeIndex].nodeState = RECEIVING;
    node[toNodeIndex].interactingNodeIndex = fromNodeIndex;
    node[toNodeIndex].endOfInteraction = currentTime + transitionDuration;
    node[toNodeIndex].transactionDataLength = dataLength;
    
    double energyConsumptionForSending = energyConsumptionOnSending(transitionDistance, dataLength, fromNodeIndex);
    double energyConsumptionForReceiving = energyConsumptionOnReceiving(dataLength, toNodeIndex);
    
    node[fromNodeIndex].energy -= energyConsumptionForSending;
    transfer_energy[fromNodeIndex] += energyConsumptionForSending;
    totalTransferEnergy += energyConsumptionForSending;
    
    node[toNodeIndex].energy -= energyConsumptionForReceiving;
    receive_energy[toNodeIndex] += energyConsumptionForReceiving;
    totalReceiveEnergy += energyConsumptionForReceiving;
    
    bits_send[fromNodeIndex] += dataLength;
}


void flushDataTransitionOnNode(int nodeIndex) {
    if (node[nodeIndex].nodeState == SENDING) {
        node[nodeIndex].cqsize -= node[nodeIndex].transactionDataLength;
    }
    if (nodeIndex < SINK_N) {
        totalArriveToSinkSize += node[nodeIndex].transactionDataLength;
    }
    node[nodeIndex].nodeState = WAITING;
    node[nodeIndex].interactingNodeIndex = INVALID_V;
    node[nodeIndex].endOfInteraction = INVALID_V;
    node[nodeIndex].transactionDataLength = 0;
}


void flushCompletedTransitions(double currentTime) {
    for (int i = 0; i < currentNodesAmount; i++) {
        if (node[i].nodeState != WAITING) {
            if (node[i].endOfInteraction < currentTime) {
                flushDataTransitionOnNode(i);
            }
        }
    }
}

void validateNodeEnergy(int nodeIndex) {
    double ethalonEnergyWaste = energyConsumptionOnSending(MAX_TRANSFER_DIS, 1, nodeIndex);
    if (node[nodeIndex].isLive) {
        if (node[nodeIndex].energy < ethalonEnergyWaste) {
            node[nodeIndex].energy = 0;
            node[nodeIndex].isLive = false;
        }
    }
}

void validateDeadNodes() {
    for (int i = SINK_N; i < currentNodesAmount; i++) { // We don't validate energy on sink nodes
        validateNodeEnergy(i);
    }
}

void checkForFirstDeathOccured(double currentTime) {
    for (int i = SINK_N; i < currentNodesAmount; i++) {
        if (!node[i].isLive) {
            firstNodeDeathOccured = currentTime;
        }
    }
}

int amountOfDataCanBeSent(int requestorIndex, int providerIndex) {
    int amountOfDataOnRequestor = node[requestorIndex].cqsize;
    int amountOfMemoryOnProvider = getRemainBuffer(providerIndex);
    int amountOfDataCanBeSentByEnergyOnRequestor = (int)round((node[requestorIndex].energy / energyConsumptionOnSending(distance(requestorIndex, providerIndex), 1, requestorIndex)));
    int amountOfDataCanBeSentByEnergyOnProvider = (int)round((node[providerIndex].energy / energyConsumptionOnReceiving(1, providerIndex)));
    int returnValue = amountOfDataOnRequestor;
    returnValue = (returnValue > amountOfMemoryOnProvider) ? amountOfMemoryOnProvider : returnValue;
    returnValue = (returnValue > amountOfDataCanBeSentByEnergyOnRequestor) ? amountOfDataCanBeSentByEnergyOnRequestor : returnValue;
    returnValue = (returnValue > amountOfDataCanBeSentByEnergyOnProvider) ? amountOfDataCanBeSentByEnergyOnProvider : returnValue;
    return returnValue;
}

#endif
