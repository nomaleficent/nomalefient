//
//  NetworkParameters.h
//  RoutingAlgo
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_NetworkParameters_h
#define RoutingAlgo_NetworkParameters_h

// Time options in seconds
#define TOTAL_MODELLING_TIME 30.0
#define TIME_QUANT 0.05

// Speed of data transfer dataquants/second
#define TRANSFER_SPEED      100

// Options of nodes amount and placing
#define WIDTH				500     // width of the area where sensor nodes exist
#define HEIGHT				500     // height of the area where sensor nodes exist
int currentNodesAmount = 	400;     // amount of nodes - should be variable
#define MAX_NODES_AMOUNT    2000    // Maximum amount of nodes during experiment
#define SINK_N              3       // amount of sink nodes
#define INVALID_V			(-1)    // Undefined value for node index
#define MAX_TRANSFER_DIS	100     // Maximal distance on which transfer is possible

// Options of input packet generations
#define PACK_INIT_RATIO		0.8     // (PACK_INIT_RATIO / 100) define the probability of one node which has pack to transfer
#define MAX_PACKET_SIZE		40      // max size of arrived packets to nodes

// Default values of nodes characteristics
#define DEFAULT_INITIAL_ENERGY              50			// get initial energy of node[i], could be set separately by initEnergy[i]
#define DEFAULT_INITIAL_BUFFER_SIZE         100         // get initial buffer size of node[i], could be set separately by initBuffer[i]
#define DEFAULT_ELECTRONIC_CONSUMPTION		5e-5//0.3        // get electronic arguments of node[i], could be set separately by eConsumption[i]
#define DEFAULT_AMPLIFIER_CONSUMPTION		1e-11//0.0002    // get amplifier arguments of node[i], could be set separately by ampConsumption[i]
#define DEFAULT_WAITING_CONSUMPTION         1e-12           // get energy waste for idle mode, cn be set separately by waitingConsumption(i), this value is per time, so idle waste per time quant = DWC * time_quant

#define NODE_AMOUNT_CHANGES_IN_EXPERIMENT 3
double changeTimes[] = {0.35, 10.5, 20.0};
int nodesDeltas[] = {-25, 30, -45};

double initialEnergy(int nodeIndex) {
    return DEFAULT_INITIAL_ENERGY;
}

int initialBufferSize(int nodeIndex) {
    return DEFAULT_INITIAL_BUFFER_SIZE;
}

double electronicConsumption(int nodeIndex) {
    return DEFAULT_ELECTRONIC_CONSUMPTION;
}

double amplifierConsumption(int nodeIndex) {
    return DEFAULT_AMPLIFIER_CONSUMPTION;
}

double waitingConsumption(int nodeIndex) {
    return DEFAULT_WAITING_CONSUMPTION;
}

// Routing weights - here just example values
#define weightOfFreeBuffer 0.2
#define weightOfHopCount 0.2
#define weightOfEnergy 0.2
#define weightOfDensity 0.2
#define weightOfEnergyCost 0.2

#endif
