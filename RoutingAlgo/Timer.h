//
//  Timer.h
//  RoutingAlgo
//
//  Created by admin on 8/1/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_Timer_h
#define RoutingAlgo_Timer_h

#include "NetworkParameters.h"
// Timer function which returns 0 after reaching TOTAL_MODELLING_TIME

double Tick() {
    static double currentTime = 0;
    currentTime += TIME_QUANT;
    if (currentTime > TOTAL_MODELLING_TIME) {
        return 0;
    }
    return currentTime;
}


#endif
