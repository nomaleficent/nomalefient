//
//  DataModel.h
//  RoutingAlgo
//
//  Created by admin on 7/18/15.
//  Copyright (c) 2015 Stanfy. All rights reserved.
//

#ifndef RoutingAlgo_DataModel_h
#define RoutingAlgo_DataModel_h

// Here you can find all data, used for modelling routing algorithm

#pragma pack(1)

// Node states
typedef enum NodeState {
    SENDING,
    RECEIVING,
    WAITING
}NodeState;

typedef struct Node {
    double energy;              // Available energy on the node
    int bufferSize;             // size of buffer
    int cqsize;                 // size of content, aka remain buffer size = bufferSize - cqsize;
    int x, y;                   // coordinate of this node
    
    bool isLive;                // whether the node is live, setting up on the en of transaction
    
    NodeState nodeState;        // Current state of node
    int interactingNodeIndex;   // Index of node, with which interaction is going on
    double endOfInteraction;    // End moment of interaction
    int transactionDataLength;  // Amount of data quants in current interaction
    
    // Last time, when packet generated
    double timeOfLastPacketArrival;
    
    // Characteristics, used for routing
    int hopCount;
    double energyCost;
} Node;

Node node[MAX_NODES_AMOUNT];                   // Actually network of nodes

//**** used for statics ****
double transfer_energy[MAX_NODES_AMOUNT];       // energy of each node, spent to transfer data
double receive_energy[MAX_NODES_AMOUNT];       // energy of each node, spent to receive data
int bits_send[MAX_NODES_AMOUNT];       // bits sent by each node
int currentDeadNodesCount = 0;     // number of dead node, calculated after updating 'isLive' property
double firstNodeDeathOccured = -1; // By default we don't have dead nodes

long totalLostPacketInArrive = 0;    // bit size lost in arrived, means, didn't go into normal node at all because of full buffer
long totalLostPacketInTransfer = 0;  // bit size lost in transfer from normal node to CH node, or from CH node to sink

double totalTransferEnergy = 0;        // total energy cost in Transfer
double totalReceiveEnergy = 0;         // total energy cost in Receive
double totalWaitingEnergyWaste = 0;

long totalArriveToSinkSize = 0;
long totalArrivalDataSize = 0;
//*****************************

#endif
